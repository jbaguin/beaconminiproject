package com.bcexpress.test;
/*
 * Description: A class to test the customer repository
 * Date created : 01-29-2019
 * Author: TeamJ3
 * 
 */
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.bcexpress.model.Customer;
import com.bcexpress.model.CustomerVO;
import com.bcexpress.repository.CustomerRepository;
import com.bcexpress.service.CustomerService;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CustomerRepositoryTest {
	
	private MockMvc mockMvc;
	private ObjectMapper objectMapper;
	
	@Mock
	CustomerRepository customerRepository;
	
	@InjectMocks
	CustomerService customerService;
	
	@Before
	public void init(){
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(customerService).build();
		objectMapper = new ObjectMapper();
		
		
	}
	
	
	@Test
	public void saveCustomerServiceTest(){
		Customer customerObj = new Customer();
		customerObj.setAddress("Cebu");
		customerObj.setAge(23);
		customerObj.setFirstName("Saitama");
		customerObj.setLastName("Hero");
		customerObj.setGender("M");
		
		/*customerService.save(customerObj);
		Customer custo = new Customer();
		custo.setAddress("Cebu");
		custo.setAge("23");
		custo.setFirstName("Saitama");
		custo.setLastName("Hero");
		custo.setGender("M");*/
		
		customerService.save(customerObj);
		Mockito.verify(customerRepository).save(Mockito.any(Customer.class));
		Mockito.verify(customerRepository, Mockito.times(1)).save(Mockito.any(Customer.class));
		
	}
	
	@Test
	public void saveRepoTest(){
		Customer customerObj = new Customer();
		customerObj.setAddress("Cebu");
		customerObj.setAge(23);
		customerObj.setFirstName("Saitama");
		customerObj.setLastName("Hero");
		customerObj.setGender("M");
		
		/*customerService.save(customerObj);
		Customer custo = new Customer();
		custo.setAddress("Cebu");
		custo.setAge("23");
		custo.setFirstName("Saitama");
		custo.setLastName("Hero");
		custo.setGender("M");*/
		
		customerRepository.save(customerObj);
		Mockito.verify(customerRepository).save(Mockito.any(Customer.class));
		Mockito.verify(customerRepository, Mockito.times(1)).save(Mockito.any(Customer.class));
		
	}
	
	
	@Test
	public void saveRepoVOTest(){
		CustomerVO customerObj = new CustomerVO();
		customerObj.setAddress("Cebu");
		customerObj.setAge("23");
		customerObj.setFirstName("Saitama");
		customerObj.setLastName("Hero");
		customerObj.setGender("M");
		customerObj.setId(20);
		
		/*customerService.save(customerObj);
		Customer custo = new Customer();
		custo.setAddress("Cebu");
		custo.setAge("23");
		custo.setFirstName("Saitama");
		custo.setLastName("Hero");
		custo.setGender("M");*/
		
		Customer cust = new Customer();
		cust.setAddress("Cebu");
		cust.setAge(23);
		cust.setFirstName("Saitama");
		cust.setLastName("Hero");
		cust.setGender("M");
		cust.setId(20);
		
		
		
		customerRepository.save(cust);
		Mockito.verify(customerRepository).save(Mockito.any(Customer.class));
		Mockito.verify(customerRepository, Mockito.times(1)).save(Mockito.any(Customer.class));
		
		assertThat(20, is(equalTo(customerObj.getId())));
		assertThat("Saitama",  is(equalTo(customerObj.getFirstName())));
		assertEquals("Hero", customerObj.getLastName());
		assertEquals("23", customerObj.getAge());
		assertEquals("M", customerObj.getGender());
		assertEquals("Cebu", customerObj.getAddress());
		
	}
	
	
	
	
	
	
	
	
	
	@Test
	public void getTest(){
		Customer customerObj = new Customer();
		customerObj.setAddress("Cebu");
		customerObj.setAge(23);
		customerObj.setFirstName("Saitama");
		customerObj.setLastName("Hero");
		customerObj.setGender("M");
		customerObj.setId(113);
		
		Mockito.when(customerRepository.findById(113)).thenReturn(customerObj);
		
		
		Customer cust = customerRepository.findById(113);
				
				
		assertThat(cust, is(notNullValue()));
		assertThat(cust, is(instanceOf(Customer.class)));
		
		assertThat(113, is(equalTo(cust.getId())));
		assertThat("Saitama",  is(equalTo(cust.getFirstName())));
		assertEquals("Hero", cust.getLastName());
		assertEquals(23, cust.getAge());
		assertEquals("M", cust.getGender());
		
		
		
	}
	
	
	@Test
	public void getCustomerServiceTest(){
		Customer customerObj = new Customer();
		customerObj.setAddress("Cebu");
		customerObj.setAge(23);
		customerObj.setFirstName("Saitama");
		customerObj.setLastName("Hero");
		customerObj.setGender("M");
		customerObj.setId(113);
		
		Mockito.when(customerService.get(113)).thenReturn(customerObj);
		
		
		Customer cust = customerRepository.findById(113);
				
				
		assertThat(cust, is(notNullValue()));
		assertThat(cust, is(instanceOf(Customer.class)));
		
		assertThat(113, is(equalTo(cust.getId())));
		assertThat("Saitama",  is(equalTo(cust.getFirstName())));
		assertEquals("Hero", cust.getLastName());
		assertEquals(23, cust.getAge());
		assertEquals("M", cust.getGender());
		assertEquals("Cebu", cust.getAddress());
		
		
		
	}
	
	

}
