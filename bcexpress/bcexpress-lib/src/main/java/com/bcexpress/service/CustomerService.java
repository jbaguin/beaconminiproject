package com.bcexpress.service;
/*
 * Description: A class that holds services will be use.
 * Date created : 01-31-2019
 * Author: TeamJ3
 * 
 * 
 * */
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bcexpress.facade.CustomerFacade;
import com.bcexpress.model.Customer;
import com.bcexpress.repository.CustomerRepository;

@Service
public class CustomerService implements CustomerFacade {

	@Autowired
	private CustomerRepository customerRepo;// call repository class to save data or retrieve data.


	/*
	 * 
	 *Service For saving the customer to the database 
	 *Input parameters: customer - Customer object for saving in database. 
	 * 					
	 * Output Value : None
	 * 
	 * */
	@Override
	@Transactional
	public void save(Customer customerObj) {

		customerRepo.save(customerObj);
	}

	/*
	 * 
	 * Service for Retrieving data from database
	 *Input parameters: id-customer ID which customer information will be retrieve. 
	 * 					
	 * Output Value : Customer informations.
	 * 
	 * */
	

	@Override
	@Transactional
	public Customer get(int id) {
	
		
		return customerRepo.findById(id);
	}

}
