package com.bcexpress.facade;
/*
 * Description: A class that manages what services will be use
 * Date created : 01-29-2019
 * Author: TeamJ3
 * 
 * 
 * */
import com.bcexpress.model.Customer;

public interface CustomerFacade {

	/*
	 * 
	 *For saving 
	 *Input parameters: customer - Customer object for saving in database. 
	 * 					
	 * Output Value : None
	 * 
	 * */
	public void save(Customer customer);
	/*
	 * 
	 * For Retrieving data from database
	 *Input parameters: id-customer ID which customer information will be retrieve. 
	 * 					
	 * Output Value : Customer informations.
	 * 
	 * */
	
	/**
	 * 
	 * @param id
	 * @return customer
	 * outputs customer object
	 */
	public Customer get(int id);

}
