package com.bcexpress.repository;
/*
 * Description: Interface class to use the JPA hibernate actions.
 * Date created : 01-29-2019
 * Author: TeamJ3
 * 
 * 
 * */
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bcexpress.model.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer,Long>{
	/*
	 * 
	 * For Retrieving data from database
	 *Input parameters: id-customer ID which customer information will be retrieve. 
	 * 					
	 * Output Value : Customer informations.
	 * 
	 * */
	public Customer findById(int id);
}
