package com.bcexpress.exceptions;
/*
 * Description: Accepts parameters for validations of customer Id.
 * Date created : 01-29-2019
 * Author: TeamJ3
 * 
 * 
 * */


public class ErrorHandlingGetCustomer {
	/*
	 * 
	 *Catch errors and validate customer ID
	 *Input parameters: ID - A variable which will be validated.
	 * Output Value : message - indicate which variable has errors.
	 * 			      description - indicate the specific problem of the message.
	 * 
	 * */
	public ErrorFormat errorMessageFormat(){
		ErrorFormat errorMessage = new ErrorFormat();//Instiate a class for storing error messages.
				
				errorMessage.setMessage("Invalid ID");
				errorMessage.setDescription(CustomExceptions.ID_NON_NUMERIC_SPECIAL_EXCEPTION);
				return errorMessage;
		
	}

}
