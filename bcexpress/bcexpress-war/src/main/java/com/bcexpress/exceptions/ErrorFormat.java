package com.bcexpress.exceptions;
/*
 * Description: Handles the format of the error messages .
 * Date created : 01-30-2019
 * Author: TeamJ3
 * 
 * 
 * */
public class ErrorFormat {
	private String message;//store for message to be pass
	private String description;//store for description to be pass
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
