package com.bcexpress.controller;
/*
 * Description:To direct in home or starting point.
 * Date created : 01-30-2019
 * Author: TeamJ3
 * 
 * 
 * */

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController {

	/*
	 * 
	 * For viewing the home page
	 *Input parameters: null request mapping. 
	 * 					
	 * Output Value : home view.
	 * 
	 * */
	@RequestMapping(value="/")
	public ModelAndView test(HttpServletResponse response) {
		return new ModelAndView("home");
	}
	
}
