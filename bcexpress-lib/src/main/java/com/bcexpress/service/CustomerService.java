package com.bcexpress.service;

/**
 * <p>
 * Action Service: <br/>
 * A class that holds customer services will be use.
 * </p>
 * * <p>
 * Return Value: customer
 * <br/>[Customer]
 * </p>
 *
 * Created on January 29, 2019
 *
 * @author TeamJ3
 */
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bcexpress.facade.CustomerFacade;
import com.bcexpress.model.Customer;
import com.bcexpress.repository.CustomerRepository;

@Service
public class CustomerService implements CustomerFacade {

	@Autowired
	private CustomerRepository customerRepo;// call repository class to save data or retrieve data.


	/**
	 * Description: Service method for saving the customer to the database 
	 * @param customerObj Customer variable 1 
	 */
	@Override
	@Transactional
	public void save(Customer customerObj) {

		customerRepo.save(customerObj);
	}

	/**
	 * Description: Service method for Retrieving data from database
	 * @param id int variable 1
	 * @return customer Customer object
	 */
	@Override
	@Transactional
	public Customer get(int id) {
	
		
		return customerRepo.findById(id);
	}

}
