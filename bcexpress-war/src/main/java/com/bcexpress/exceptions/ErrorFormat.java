package com.bcexpress.exceptions;

/**
 * <p>
 * Handles the format of the error messages .
 * </p>
 * 
 * <br/>
 * Created on January 29, 2019
 * 
 * @author TeamJ3
 */
public class ErrorFormat {
	private String message;//store for message to be pass
	private String description;//store for description to be pass
	/**
	 * Description: Method to get message of error
	 * @return message String
	 */	
	public String getMessage() {
		return message;
	}
	/**
	 * Description: Method to set message of error
	 * @param message String variable 1
	 */	
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * Description: Method to get description of error
	 * @return description String 
	 */	
	public String getDescription() {
		return description;
	}
	/**
	 * Description: Method to set description of error
	 * @param description String variable 1
	 */	
	public void setDescription(String description) {
		this.description = description;
	}
}
