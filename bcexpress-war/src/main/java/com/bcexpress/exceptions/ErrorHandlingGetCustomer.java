package com.bcexpress.exceptions;

/**
 * <p>
 * Accepts parameters for validations of data to be save in database.
 * </p>
 * 
 * <br/>
 * Created on January 29, 2019
 * 
 * @author TeamJ3
 */
public class ErrorHandlingGetCustomer {
	/**
	 * Description: Method to get description of error
	 * @return errorMessage ErrorFormat  
	 */	
	public ErrorFormat errorMessageFormat(){
		ErrorFormat errorMessage = new ErrorFormat();//Instiate a class for storing error messages.
				
				errorMessage.setMessage("Invalid ID");
				errorMessage.setDescription(CustomExceptions.ID_NON_NUMERIC_SPECIAL_EXCEPTION);
				return errorMessage;
		
	}

}
